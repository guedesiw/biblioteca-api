package br.com.faculdadedelta.bibliotecaapi.resource;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.faculdadedelta.bibliotecaapi.model.BaseModel;
import br.com.faculdadedelta.bibliotecaapi.service.BaseService;

@SuppressWarnings("rawtypes")
public abstract class BaseResource<E extends BaseModel, S extends BaseService, R extends JpaRepository> {
	
	@Autowired
	private S service;
	
	@Autowired
	private R repository;
	
	@SuppressWarnings("unchecked")
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public E inserir(@RequestBody @Valid E entidade, HttpServletResponse response) throws Throwable {
		consistirAtributoNomeEntidade(entidade);
		E entidadeCadastrada = (E) this.service.inserir(entidade);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/id").buildAndExpand(entidadeCadastrada.getId()).toUri();
		
		response.setHeader(HttpHeaders.LOCATION, uri.toString());
		
		return entidadeCadastrada;
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<E> listar(){
		return service.listar();
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public E pesquisarPorId(@PathVariable("id") Long id, HttpServletResponse response) throws Throwable {
		E entidadePesquisada = (E) service.pesquisarPorId(id, StringUtils.replace(this.getClass().getName(), "Resource", ""));
		return entidadePesquisada;
	}
	
	@SuppressWarnings("unchecked")
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public E alterar(@RequestBody @Valid E entidade, @PathVariable("id") Long id) throws Throwable {
		consistirAtributoNomeEntidade(entidade);
		return (E) service.alterar(entidade, id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void excluir(@PathVariable("id") Long id) throws Throwable{
		service.excluir(id);
	}
	
	@SuppressWarnings("unchecked")
	public void consistirEntidade(E entidade) throws Throwable{
		String[] nomeDaClasse = StringUtils.split(entidade.getClass().getName(), ".");
		String mensagem;
		if(entidade != null && entidade.getId() != null) {
			mensagem = new String("Não existe "+nomeDaClasse[nomeDaClasse.length - 1].toLowerCase()+" cadastrado(a) com o id: "+entidade.getId());
		}else {
			mensagem = "O ID do(a) "+nomeDaClasse[nomeDaClasse.length - 1].toLowerCase()+" está nulo!";
		}
		repository.findById(entidade.getId()).orElseThrow(() -> new EmptyResultDataAccessException(mensagem, 1));
	}
	
	public void consistirAtributoNomeEntidade(E entidade) throws Throwable{
		if(entidade.consistirNome() && (entidade.getNome() == null || entidade.getNome().isEmpty())) {
			throw new EmptyResultDataAccessException("O nome é obrigatório!", 1);
		}
	}
}
