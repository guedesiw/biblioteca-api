package br.com.faculdadedelta.bibliotecaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.faculdadedelta.bibliotecaapi.model.Cliente;
import br.com.faculdadedelta.bibliotecaapi.repository.ClienteRepository;
import br.com.faculdadedelta.bibliotecaapi.service.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteResource extends BaseResource<Cliente, ClienteService, ClienteRepository>{

	@Autowired
	private EnderecoResource enderecoResource;
	
	@Override
	public Cliente inserir(@RequestBody @Valid Cliente entidade, HttpServletResponse response) throws Throwable {
		enderecoResource.consistirEntidade(entidade.getEndereco());
		return super.inserir(entidade, response);
	}
	
	@Override
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Cliente alterar(@RequestBody @Valid Cliente entidade, @PathVariable("id") Long id) throws Throwable {
		enderecoResource.consistirEntidade(entidade.getEndereco());
		return super.alterar(entidade, id);
	}
	
	@Override
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void excluir(@PathVariable("id") Long id) throws Throwable {
		Long enderecoId = pesquisarPorId(id, null).getEndereco().getId();
		super.excluir(id);
		enderecoResource.excluir(enderecoId);
	}
}
