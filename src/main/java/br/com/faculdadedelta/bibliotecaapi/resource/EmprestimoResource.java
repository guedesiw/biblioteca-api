package br.com.faculdadedelta.bibliotecaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.faculdadedelta.bibliotecaapi.model.Emprestimo;
import br.com.faculdadedelta.bibliotecaapi.repository.EmprestimoRepository;
import br.com.faculdadedelta.bibliotecaapi.service.EmprestimoService;

@RestController
@RequestMapping("/emprestimos")
public class EmprestimoResource extends BaseResource<Emprestimo, EmprestimoService, EmprestimoRepository>{
	
	@Autowired
	private LivroResource livroResource;
	
	@Autowired
	private ClienteResource clienteResource;
	
	@Override
	public Emprestimo inserir(@Valid Emprestimo entidade, HttpServletResponse response) throws Throwable {
		livroResource.consistirEntidade(entidade.getLivro());
		clienteResource.consistirEntidade(entidade.getCliente());
		return super.inserir(entidade, response);
	}
	
	@Override
	public Emprestimo alterar(@Valid Emprestimo entidade, Long id) throws Throwable {
		livroResource.consistirEntidade(entidade.getLivro());
		clienteResource.consistirEntidade(entidade.getCliente());
		return super.alterar(entidade, id);
	}
}
