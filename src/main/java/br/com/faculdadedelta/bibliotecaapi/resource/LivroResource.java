package br.com.faculdadedelta.bibliotecaapi.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.faculdadedelta.bibliotecaapi.model.Livro;
import br.com.faculdadedelta.bibliotecaapi.repository.LivroRepository;
import br.com.faculdadedelta.bibliotecaapi.service.LivroService;

@RestController
@RequestMapping("/livros")
public class LivroResource extends BaseResource<Livro, LivroService, LivroRepository> {

	@Autowired
	private GeneroResource generoResource;
	
	@Autowired
	private AutorResource autorResource;
	
	@Autowired
	private EditoraResource editoraResource;

	@Override
	public Livro inserir(@RequestBody @Valid Livro entidade, HttpServletResponse response) throws Throwable {
		generoResource.consistirEntidade(entidade.getGenero());
		autorResource.consistirEntidade(entidade.getAutor());
		editoraResource.consistirEntidade(entidade.getEditora());
		return super.inserir(entidade, response);
	}

	@Override
	public Livro alterar(@RequestBody @Valid Livro entidade, Long id) throws Throwable {
		generoResource.consistirEntidade(entidade.getGenero());
		autorResource.consistirEntidade(entidade.getAutor());
		editoraResource.consistirEntidade(entidade.getEditora());
		return super.alterar(entidade, id);
	}
}
