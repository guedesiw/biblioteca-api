package br.com.faculdadedelta.bibliotecaapi.service;

import org.springframework.stereotype.Service;

import br.com.faculdadedelta.bibliotecaapi.model.Genero;
import br.com.faculdadedelta.bibliotecaapi.repository.GeneroRepository;

@Service
public class GeneroService extends BaseService<Genero, GeneroRepository> {
	
}
