package br.com.faculdadedelta.bibliotecaapi.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.faculdadedelta.bibliotecaapi.model.BaseModel;

public abstract class BaseService<E extends BaseModel<E>, R extends JpaRepository<E, Long>> {
	
	@Autowired
	private R repository;

	@Transactional
	public E inserir(E entidade) {
		entidade.setId(null);
		return repository.save(entidade);
	}

	public E pesquisarPorId(Long id, String className) throws Throwable {
		if(className != null) {
			String[] arrayClassName = StringUtils.split(className, ".");
			return repository.findById(id).orElseThrow(() -> new EmptyResultDataAccessException("Não foi possível localizar o(a) "+arrayClassName[arrayClassName.length - 1].toLowerCase()+" pelo id "+id, 1));
		}else {
			return repository.findById(id).orElseThrow(() -> new EmptyResultDataAccessException(1));
		}
	}
	
	@Transactional
	public E alterar(E entidade, Long id) throws Throwable {
		E entidadePesquisada = pesquisarPorId(id, entidade.getClass().getName());
		BeanUtils.copyProperties(entidade, entidadePesquisada, "id");
		return repository.save(entidadePesquisada);
	}

	@Transactional
	public void excluir(Long id) {
		repository.deleteById(id);
	}

	public List<E> listar() {
		return repository.findAll();
	}
}
