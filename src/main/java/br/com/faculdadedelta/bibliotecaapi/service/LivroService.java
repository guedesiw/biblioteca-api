package br.com.faculdadedelta.bibliotecaapi.service;

import org.springframework.stereotype.Service;

import br.com.faculdadedelta.bibliotecaapi.model.Livro;
import br.com.faculdadedelta.bibliotecaapi.repository.LivroRepository;

@Service
public class LivroService extends BaseService<Livro, LivroRepository> {
	
}
