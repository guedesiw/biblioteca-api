package br.com.faculdadedelta.bibliotecaapi.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Cliente extends BaseModel<Cliente> {
	
	@NotBlank(message = "O CPF é obrigatório!")
	private String cpf;
	
	@NotBlank(message = "O e-mail é obrigatório!")
	private String email;
	
	@NotBlank(message = "O telefone é obrigatório!")
	private String telefone;
	
	@NotNull(message = "O endereco é obrigatório!")
	@ManyToOne
	@JoinColumn(name = "id_endereco")
	private Endereco endereco;
	
	public Cliente() {
		super();
	}
	
	public Cliente(Long id, String nome, String cpf, String email, String telefone, Endereco endereco) {
		super(id,nome);
		this.cpf = cpf;
		this.email = email;
		this.telefone = telefone;
		this.endereco = endereco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
