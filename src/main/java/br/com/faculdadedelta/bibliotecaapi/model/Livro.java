package br.com.faculdadedelta.bibliotecaapi.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Livro extends BaseModel<Livro> {

	@NotNull(message = "O volume é obrigatório!")
	private Integer volume;

	@NotNull(message = "A data de publicação é obrigatória!")
	private LocalDate dataPublicacao;

	@NotNull(message = "O valor é obrigatório!")
	private BigDecimal valor;

	@NotNull(message = "O genero é obrigatório!")
	@ManyToOne
	@JoinColumn(name = "id_genero")
	private Genero genero;

	@NotNull(message = "A editora é obrigatória!")
	@ManyToOne
	@JoinColumn(name = "id_editora")
	private Editora editora;

	@NotNull(message = "O autor é obrigatório!")
	@ManyToOne
	@JoinColumn(name = "id_autor")
	private Autor autor;

	public Livro() {
		super();
	}

	public Livro(Long id, String nome, Integer volume, LocalDate dataPublicacao, BigDecimal valor, Genero genero) {
		super(id, nome);
		this.volume = volume;
		this.dataPublicacao = dataPublicacao;
		this.valor = valor;
		this.genero = genero;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public LocalDate getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(LocalDate dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Editora getEditora() {
		return editora;
	}

	public void setEditora(Editora editora) {
		this.editora = editora;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}
}