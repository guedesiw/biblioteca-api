package br.com.faculdadedelta.bibliotecaapi.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Emprestimo extends BaseModel<Emprestimo> {

	@NotNull(message = "A data de emprestimo é obrigatória!")
	private LocalDateTime dataEmprestimo;

	@NotNull(message = "A data de devolução é obrigatória!")
	private LocalDateTime dataDevolucao;

	@NotNull(message = "O valor do emprestimo é obrigatório!")
	private BigDecimal valorDoEmprestimo;

	@NotNull(message = "O livro é obrigatório!")
	@ManyToOne
	@JoinColumn(name = "id_livro")
	private Livro livro;

	@NotNull(message = "O cliente é obrigatório!")
	@ManyToOne
	@JoinColumn(name = "id_cliente")
	private Cliente cliente;

	public Emprestimo() {
		super();
	}

	public Emprestimo(Long id, LocalDateTime dataEmprestimo, LocalDateTime dataDevolucao, BigDecimal valorDoEmprestimo,
			Livro livro, Cliente cliente) {
		super(id);
		this.dataEmprestimo = dataEmprestimo;
		this.dataDevolucao = dataDevolucao;
		this.valorDoEmprestimo = valorDoEmprestimo;
		this.livro = livro;
	}

	public LocalDateTime getDataEmprestimo() {
		return dataEmprestimo;
	}

	public void setDataEmprestimo(LocalDateTime dataEmprestimo) {
		this.dataEmprestimo = dataEmprestimo;
	}

	public LocalDateTime getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(LocalDateTime dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public BigDecimal getValorDoEmprestimo() {
		return valorDoEmprestimo;
	}

	public void setValorDoEmprestimo(BigDecimal valorDoEmprestimo) {
		this.valorDoEmprestimo = valorDoEmprestimo;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
