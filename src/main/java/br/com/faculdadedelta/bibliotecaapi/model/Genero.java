package br.com.faculdadedelta.bibliotecaapi.model;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
public class Genero extends BaseModel<Genero> {

	@NotBlank(message = "A descrição é obrigatória!")
	private String descricao;

	public Genero() {
		super();
	}

	public Genero(Long id, String descricao) {
		super(id);
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}