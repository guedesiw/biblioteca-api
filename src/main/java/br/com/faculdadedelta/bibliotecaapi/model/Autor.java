package br.com.faculdadedelta.bibliotecaapi.model;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import br.com.faculdadedelta.bibliotecaapi.type.Sexo;

@Entity
public class Autor extends BaseModel<Autor>{
	
	@NotNull(message = "O sexo é obrigatório!")
	private Sexo sexo;
	
	public Autor() {
		super();
	}

	public Autor(Long id, String nome, Sexo sexo) {
		super(id, nome);
		this.sexo = sexo;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
}
