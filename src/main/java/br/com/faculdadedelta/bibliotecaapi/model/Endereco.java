package br.com.faculdadedelta.bibliotecaapi.model;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
public class Endereco extends BaseModel<Endereco> {
	
	@NotBlank(message = "A rua é obrigatória!")
	private String rua;
	
	@NotBlank(message = "O bairro é obrigatório!")
	private String bairro;
	
	@NotBlank(message = "A quadra é obrigatória!")
	private String quadra;
	
	@NotBlank(message = "O lote é obrigatório!")
	private String lote;
	
	@NotBlank(message = "O numero é obrigatório!")
	private String numero;
	
	@NotBlank(message = "O complemento é obrigatório!")
	private String complemento;
	
	@NotBlank(message = "A cidade é obrigatória!")
	private String cidade;
	
	@NotBlank(message = "O estado é obrigatório!")
	private String estado;
	
	@NotBlank(message = "O pais é obrigatório!")
	private String pais;
	
	public Endereco() {
		super();
	}

	public Endereco(Long id, String rua, String bairro, String quadra, String lote, String numero, String complemento,
			String cidade, String estado, String pais) {
		super(id);
		this.rua = rua;
		this.bairro = bairro;
		this.quadra = quadra;
		this.lote = lote;
		this.numero = numero;
		this.complemento = complemento;
		this.cidade = cidade;
		this.estado = estado;
		this.pais = pais;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getQuadra() {
		return quadra;
	}

	public void setQuadra(String quadra) {
		this.quadra = quadra;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

}
