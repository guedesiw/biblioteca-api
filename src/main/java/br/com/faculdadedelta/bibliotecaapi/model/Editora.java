package br.com.faculdadedelta.bibliotecaapi.model;

import javax.persistence.Entity;

@Entity
public class Editora extends BaseModel<Editora>{

	public Editora() {
		super();
	}

	public Editora(Long id, String nome) {
		super(id, nome);
	}

}
