package br.com.faculdadedelta.bibliotecaapi.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApiException extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler({EmptyResultDataAccessException.class})
	public ResponseEntity<Object> handlerEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request){
		String mensagemUsuario = ex.getMessage();
		if(mensagemUsuario == null || mensagemUsuario.isEmpty()) {
			mensagemUsuario = "Recurso não encontrado!";
		}
		if(StringUtils.contains(mensagemUsuario, "No class") && //Ex: No class br.com.faculdadedelta.bibliotecaapi.model.Livro entity with id 1 exists!
		   StringUtils.contains(mensagemUsuario, "entity with id") && 
		   StringUtils.contains(mensagemUsuario, "exists!") ) {
			String[] arrayEntidade = StringUtils.split(StringUtils.substringBetween(mensagemUsuario, "No class", "entity with id"),".");
			mensagemUsuario = "Não existe um(a) "+ arrayEntidade[arrayEntidade.length - 1].toLowerCase().trim()+ " com o id "+RegExUtils.removeAll(mensagemUsuario, "[^\\d]").trim();
		}
		if(StringUtils.contains(mensagemUsuario, "Incorrect result size")) {//Ex: Incorrect result size: expected 1, actual 0
			mensagemUsuario = "Não houve resultados na pesquisa realizada!";
		}
		String mensagemDesenvolvedor = ex.toString();
		List<ErroDetalhe> errors = Arrays.asList(new ErroDetalhe(mensagemUsuario, mensagemDesenvolvedor));
		return handleExceptionInternal(ex, errors, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	private List<ErroDetalhe> criarListaErros(BindingResult bindingResult){
		List<ErroDetalhe> erros = new ArrayList<ApiException.ErroDetalhe>();
		
//		for(FieldError fieldError : bindingResult.getFieldErrors()) {
//			erros.add(new ErroDetalhe(fieldError.getDefaultMessage(), fieldError.toString()));
//		}
		
		bindingResult.getFieldErrors().forEach((fieldErro) -> erros.add(new ErroDetalhe(fieldErro.getDefaultMessage(), fieldErro.toString())));
		
		return erros;
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		List<ErroDetalhe> erros = criarListaErros(ex.getBindingResult());
		
		return handleExceptionInternal(ex, erros, headers, status, request);
	}
	
	public static class ErroDetalhe{
		private String mensagemUsuario;
		private String mensagemDesenvolvedor;
		
		public ErroDetalhe(String mensagemUsuario, String mensagemDesenvolvedor) {
			this.mensagemUsuario = mensagemUsuario;
			this.mensagemDesenvolvedor = mensagemDesenvolvedor;
		}
		
		public String getMensagemUsuario() {
			return mensagemUsuario;
		}
		public void setMensagemUsuario(String mensagemUsuario) {
			this.mensagemUsuario = mensagemUsuario;
		}
		public String getMensagemDesenvolvedor() {
			return mensagemDesenvolvedor;
		}
		public void setMensagemDesenvolvedor(String mensagemDesenvolvedor) {
			this.mensagemDesenvolvedor = mensagemDesenvolvedor;
		}
	}
}
